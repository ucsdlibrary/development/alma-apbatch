require_relative "../../app/email"
require "json"

RSpec.describe App::Email do
  include Mail::Matchers

  context "successful email" do
    let(:xml_file) { File.read("spec/fixtures/sampleAlmaXml.xml") }
    let(:oracle_json_file) { JSON.parse(File.read("spec/fixtures/sampleOracleExport.json")) }
    let(:csv_output_file) { File.read("spec/fixtures/csv_test_fixture.csv") }
    let(:xml_location_example) { "APprocessed/sampleAlmaXml.xml" }

    before do
      Mail::TestMailer.deliveries.clear
      described_class.send(
        csv_attachment: csv_output_file,
        json_attachment: oracle_json_file,
        xml_location: xml_location_example,
        xml_attachment: xml_file
      )
    end

    describe "sending an successful email with required attachments" do
      it { is_expected.to have_sent_email }
      it {
        is_expected.to have_sent_email.with_attachments([
          an_attachment_with_filename("oracle-export.json"),
          an_attachment_with_filename("alma-export.xml"),
          an_attachment_with_filename("csv-report.csv")
        ])
      }
    end
  end

  context "failure email" do
    let(:xml_location_example) { "apbatch/APprocessed/errors/sampleAlmaXml.xml" }
    let(:errors) { ["error1", "error2", "error3"] }
    before do
      Mail::TestMailer.deliveries.clear
      described_class.send_error(
        xml_location: xml_location_example,
        errors: errors
      )
    end

    describe "sending a failure email with reference to failed file" do
      it { is_expected.to have_sent_email }
      it {
        is_expected.to have_sent_email.with_attachments([
          an_attachment_with_filename("errors.txt")
        ])
      }
    end
  end
end
