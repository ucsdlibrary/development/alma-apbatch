require_relative "../../app/upload"

RSpec.describe App::Upload do
  let(:oracle_json_file) { JSON.parse(File.read("spec/fixtures/sampleJsonPayload.json")) }
  let(:token_uri) { ENV.fetch("OFC_TOKEN_URL") }
  let(:api_uri) { ENV.fetch("OFC_API_URL") }
  let(:headers) do
    {
      "Authorization" => "Basic ".concat(ENV.fetch("OFC_CLIENT_SECRET")),
      "Content-Type" => "application/x-www-form-urlencoded"
    }
  end
  let(:api_headers) do
    {
      "Authorization" => "Bearer 1234",
      "Content-Type" => "application/json"
    }
  end
  let(:response_body) { '{"access_token":"1234","scope":"am_application_scope default","token_type":"Bearer","expires_in":9223372036854775}' }
  # subject { described_class.new }

  before do
    stub_const("ENV", ENV.to_hash.merge("OFC_TOKEN_URL" => "http://example.com/token"))
    stub_const("ENV", ENV.to_hash.merge("OFC_CLIENT_SECRET" => "some_value"))
    stub_const("ENV", ENV.to_hash.merge("OFC_API_URL" => "http://api-example.com"))
  end

  describe "NullUpload for local development" do
    before do
      stub_const("ENV", ENV.to_hash.merge("NULL_UPLOAD" => "yes"))
    end
    it "can use NullUpload when NULL_UPLOAD env var is present" do
      expect { described_class.for.send }.to output(/NullUpload/).to_stdout
    end
  end

  describe "interacting with OFC API endpoints" do
    before do
      stub_request(:post, "#{token_uri}?grant_type=client_credentials")
        .with(body: {"{}" => nil}, headers: headers)
        .to_return(status: 200, body: response_body, headers: {})
      stub_request(:post, api_uri)
        .with(body: JSON.generate(oracle_json_file), headers: api_headers)
        .to_return(status: 200, body: "", headers: {})
    end
    it "can get an access token" do
      expect(described_class.instance.token).to eq("1234")
    end

    it "can upload invoice data" do
      expect(described_class.instance.send(json_attachment: JSON.generate(oracle_json_file))).to be_truthy
    end
  end
end
