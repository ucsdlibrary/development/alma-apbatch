require_relative "../../app/transform"

RSpec.describe App::Transform do
  let(:fixture_file) { File.open("spec/fixtures/sampleAlmaXmlTax.xml") }
  subject { described_class.new(file: fixture_file) }

  describe "#extract_invoices for taxable invoice" do
    before { subject.extract_invoices }

    it "assigns taxable field values to an Invoice record" do
      expect(subject.invoices.first.termsName).to eq("Immediate")
      expect(subject.invoices.first.calcTaxDuringImportFlag).to eq("N")
      expect(subject.invoices.first.addTaxToInvAmtFlag).to eq("N")
    end

    it "assigns taxable field values and different pjcExpenditureTypeName to Taxable InvoiceLine records" do
      fourth_invoice_lines = subject.invoices.first.invoice_lines[3]
      expect(fourth_invoice_lines.pjcExpenditureTypeName).to eq("526300 - Capital Exp - Library Use Only-Books/Matl")
      expect(fourth_invoice_lines.shipToLocationCode).to eq("UCSD")
      expect(fourth_invoice_lines.attribute12).to eq("No")
    end

    it "tracks all Invoice records" do
      expect(subject.invoices.count).to be(1)
    end
  end
end
