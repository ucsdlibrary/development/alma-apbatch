require_relative "../../app/oracle_export"
require_relative "../../app/transform"

RSpec.describe App::OracleExport do
  let(:fixture_file) { File.open("spec/fixtures/sampleAlmaXml.xml") }
  let(:batch) { App::Transform.new(file: fixture_file) }
  let(:export) { described_class.new(invoices: batch.invoices) }
  before do
    batch.extract_invoices
  end

  context "convert to hash format" do
    it "contains data ApInvoicesInterface" do
      expect(export.build_data[0][described_class::INVOICES_KEY][0])
        .to include(invoiceId: "026197560006535",
          businessUnit: "UCSD CAMPUS",
          invoiceNum: "036-23270102-LIB",
          invoiceAmount: "649.55",
          invoiceDate: "2021-05-31",
          vendorNum: "17648",
          vendorSiteCode: "PAYMT ACH 1")
    end

    it "contains data for ApInvoiceLinesInterface" do
      expect(export.build_data[1][described_class::INVOICE_LINES_KEY][0])
        .to include(invoiceId: "026197560006535",
          amount: "204.5",
          lineNumber: "1",
          pjcTaskNumber: "4",
          pjcExpenditureItemDate: "2021-05-31",
          distCodeConcatenated: "16110.13991.1000229.526300.600.000.000000.1023569.000000.000000.000000.000000",
          pjcExpenditureTypeName: "526300 - Capital Exp - Library Use Only-Books/Matl")
    end

    it "includes constant attributes in ApInvoicesInterface" do
      expect(export.build_data[0][described_class::INVOICES_KEY][0])
        .to include(invoiceCurrencyCode: "USD",
          paymentCurrencyCode: "USD",
          invoiceType: "STANDARD",
          vendorName: "",
          source: "UCSD Int Library Sys")
    end

    it "includes constant attributes in ApInvoiceLinesInterface" do
      expect(export.build_data[1][described_class::INVOICE_LINES_KEY][0])
        .to include(lineTypeLookupCode: "ITEM",
          invoiceCurrencyCode: "USD",
          paymentCurrencyCode: "USD",
          pjcOrganizationName: "Library Collections")
    end
  end

  context "convert to JSON format" do
    let(:invoice) do
      {"invoiceId" => "026197560006535",
       "businessUnit" => "UCSD CAMPUS",
       "invoiceNum" => "036-23270102-LIB",
       "invoiceAmount" => "649.55",
       "invoiceDate" => "2021-05-31",
       "vendorNum" => "17648",
       "vendorSiteCode" => "PAYMT ACH 1"}
    end

    let(:invoice_lines) do
      {"invoiceId" => "026197560006535",
       "amount" => "204.5",
       "lineNumber" => "1",
       "pjcTaskNumber" => "4",
       "pjcExpenditureItemDate" => "2021-05-31",
       "distCodeConcatenated" => "16110.13991.1000229.526300.600.000.000000.1023569.000000.000000.000000.000000",
       "pjcExpenditureTypeName" => "526300 - Capital Exp - Library Use Only-Books/Matl"}
    end

    it "contains JSON data ApInvoicesInterface" do
      expect(JSON.parse(export.to_oracle_invoice)[0][described_class::INVOICES_KEY][0].to_h)
        .to include(invoice)
    end

    it "contains data for ApInvoiceLinesInterface" do
      expect(JSON.parse(export.to_oracle_invoice)[1][described_class::INVOICE_LINES_KEY][0].to_h)
        .to include(invoice_lines)
    end
  end
end
