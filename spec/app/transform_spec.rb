require_relative "../../app/transform"

RSpec.describe App::Transform do
  context "with invalid xml input" do
    describe "invalid line types" do
      let(:fixture_file) { File.open("spec/fixtures/invoice_export_#29106.xml") }
      subject { described_class.new(file: fixture_file) }

      before { subject.extract_invoices }

      it "filters out invalid line types" do
        expect(subject.invoices.first.invoice_lines.count).to be(2)
      end
    end

    describe "validation failures" do
      let(:fixture_file) { File.open("spec/fixtures/validation_failures.xml") }
      subject { described_class.new(file: fixture_file) }

      before { subject.extract_invoices }

      it "logs all validation errors" do
        expect(subject.flattened_errors.count).to be(4)
      end

      it "includes identifiable information about each validation error" do
        expect(subject.flattened_errors).to include(match(/unique_identifier is not present/))
        expect(subject.flattened_errors).to include(match(/line_number is not present in invoice line/))
        expect(subject.flattened_errors).to include(match(/external_id is not present in invoice line/))
        expect(subject.flattened_errors).to include(match(/vendor_additional_code is not present/))
      end
    end
  end

  context "with valid xml input" do
    let(:fixture_file) { File.open("spec/fixtures/sampleAlmaXml.xml") }
    subject { described_class.new(file: fixture_file) }

    describe "#extract_invoices" do
      before { subject.extract_invoices }

      it "assigns values to an Invoice record" do
        expect(subject.invoices.first.invoiceNum).to eq("036-23270102-LIB")
        expect(subject.invoices.first.invoiceId).to eq("026197560006535")
        expect(subject.invoices.first.invoiceAmount).to eq("649.55")
        expect(subject.invoices.first.businessUnit).to eq("UCSD CAMPUS")
        expect(subject.invoices.first.vendorNum).to eq("17648")
        expect(subject.invoices.first.vendorSiteCode).to eq("PAYMT ACH 1")
        expect(subject.invoices.first.invoiceDate).to eq("2021-05-31")
      end

      it "does not have taxable fields" do
        expect(subject.invoices.first.termsName).to be(nil)
        expect(subject.invoices.first.calcTaxDuringImportFlag).to be(nil)
        expect(subject.invoices.first.addTaxToInvAmtFlag).to be(nil)
      end

      it "assign line numbers for an InvoiceLine" do
        expect(subject.invoices.first.invoice_lines.map(&:lineNumber))
          .to contain_exactly("1", "2", "3", "4", "5", "6")
      end

      it "assigns InvoiceLine values for an Invoice" do
        first_invoice_lines = subject.invoices.first.invoice_lines.first
        expect(first_invoice_lines.amount).to eq("106.65")
        expect(first_invoice_lines.lineTypeLookupCode).to eq("ITEM")
        expect(first_invoice_lines.invoiceCurrencyCode).to eq("USD")
        expect(first_invoice_lines.paymentCurrencyCode).to eq("USD")
        expect(first_invoice_lines.pjcOrganizationName).to eq("Library Collections")
        expect(first_invoice_lines.pjcExpenditureItemDate).to eq("2021-05-31")
      end

      it "assigns the same Invoice#invoiceId to each InvoiceLine#invoiceId" do
        expect(subject.invoices.first.invoice_lines.map(&:invoiceId).uniq)
          .to eq([subject.invoices.first.invoiceId])
      end

      it "assigns ChargeOfAccount external_id fields to InvoiceLine records" do
        first_invoice_lines = subject.invoices.first.invoice_lines.first
        expect(first_invoice_lines.distCodeConcatenated).to eq("16110.13991.1000229.526300.600.000.000000.1023569.000000.00000.000000.000000")
        expect(first_invoice_lines.pjcProjectNumber).to eq("1023569")
        expect(first_invoice_lines.pjcTaskNumber).to eq("4")
        expect(first_invoice_lines.pjcExpenditureTypeName).to eq("526300 - Capital Exp - Library Use Only-Books/Matl")
        expect(first_invoice_lines.shipToLocationCode).to be(nil)
        expect(first_invoice_lines.attribute12).to be(nil)
      end

      it "tracks all Invoice records" do
        expect(subject.invoices.count).to be(1)
      end
    end
  end
end
