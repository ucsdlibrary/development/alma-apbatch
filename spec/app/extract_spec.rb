require_relative "../../app/extract"

RSpec.describe App::Extract do
  subject { described_class.new }
  describe "#run" do
    let(:stubbed_bucket_listing) do
      {contents: [
        {key: "test.xml"},
        {key: "test2.xml"},
        {key: ENV["KEEP_FILE"]}
      ]}
    end
    let(:stub_s3_client) do
      Aws::S3::Client.new(stub_responses: {
        list_objects_v2: stubbed_bucket_listing
      })
    end

    before do
      allow(App::S3Client).to receive(:client).and_return(stub_s3_client)
      allow(App::Transaction).to receive(:call).with(anything).and_return("nothing")
    end

    it "does not process the KEEP_FILE" do
      expect(App::Transaction).to receive(:call).twice # stubbed_bucket_listing size - KEEP_FILE
      subject.run
    end
  end
end
