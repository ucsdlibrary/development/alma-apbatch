require_relative "../../app/csv_report"
require_relative "../../app/transform"

RSpec.describe App::CsvReport do
  let(:xml_input_file) { File.open("spec/fixtures/sampleAlmaXml.xml") }
  let(:csv_output_file) { File.read("spec/fixtures/csv_test_fixture.csv") }
  let(:batch) { App::Transform.new(file: xml_input_file) }
  let(:report) { described_class.create(invoices: batch.invoices) }

  before do
    batch.extract_invoices
  end

  it "returns a CSV report with the desired properties" do
    expect(report).to eq(csv_output_file)
  end
end
