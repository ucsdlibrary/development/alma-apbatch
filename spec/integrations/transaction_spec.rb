require_relative "../../app/extract"
require_relative "../../app/s3_client"
require "aws-sdk-s3"

RSpec.describe "Transactions" do
  subject { App::Extract.new }
  let(:files_to_process) { ["invoice_export_#2201091.xml", "invoice_export_#29106.xml", "invoice_export_#61585.xml"] }
  let(:files_to_fail) { ["epo_sample.xml"] }

  before do
    App::S3Client.client.create_bucket({
      bucket: ENV["MINIO_BUCKET"],
      acl: "private"
    })
    # Setup files to process
    (files_to_process + files_to_fail).each do |xml_fixture|
      File.open("spec/fixtures/#{xml_fixture}", "rb") do |file|
        App::S3Client.client.put_object({
          body: file,
          bucket: ENV.fetch("MINIO_BUCKET"),
          key: "#{ENV.fetch("MINIO_KEY_PREFIX")}/#{File.basename(xml_fixture)}"
        })
      end
    end
    # Setup KEEP_FILE
    App::S3Client.client.put_object({
      body: "keep me",
      bucket: ENV.fetch("MINIO_BUCKET"),
      key: "#{ENV.fetch("MINIO_KEY_PREFIX")}/#{ENV["KEEP_FILE"]}"
    })
  end

  after do
    objects_to_delete = App::S3Client.client.list_objects_v2({bucket: ENV["MINIO_BUCKET"]}).contents.map { |obj| {key: obj.key} }
    App::S3Client.client.delete_objects({
      bucket: ENV["MINIO_BUCKET"],
      delete: {
        objects: objects_to_delete,
        quiet: true
      }
    })
    App::S3Client.client.delete_bucket({bucket: ENV["MINIO_BUCKET"]})
  end

  describe "file processing on MINIO_BUCKET" do
    before do
      App::Extract.new.run
    end

    it "always leaves the KEEP_FILE in place" do
      files_remaining = App::S3Client.client.list_objects_v2(
        bucket: ENV.fetch("MINIO_BUCKET"),
        prefix: ENV.fetch("MINIO_KEY_PREFIX")
      ).contents
      expect(files_remaining.size).to be(1)
      expect(files_remaining.first.key).to eq("#{ENV["MINIO_KEY_PREFIX"]}/#{ENV["KEEP_FILE"]}")
    end

    it "moves successfully processed files to MINIO_PROCESSED_KEY_PREFIX" do
      files_processed = App::S3Client.client.list_objects_v2(
        bucket: ENV.fetch("MINIO_BUCKET"),
        prefix: ENV.fetch("MINIO_PROCESSED_KEY_PREFIX")
      ).contents
      expect(files_processed.size).to be(files_to_process.size)
      files_processed_keys = files_processed.map { |obj| obj.key.split("/").last }
      expect(files_processed_keys.sort).to eq(files_to_process.sort)
    end

    it "moves files that can't be processed to MINIO_ERRORS_KEY_PREFIX" do
      files_failed = App::S3Client.client.list_objects_v2(
        bucket: ENV.fetch("MINIO_BUCKET"),
        prefix: ENV.fetch("MINIO_ERRORS_KEY_PREFIX")
      ).contents
      expect(files_failed.size).to be(files_to_fail.size)
      files_failed_keys = files_failed.map { |obj| obj.key.split("/").last }
      expect(files_failed_keys.sort).to eq(files_to_fail.sort)
    end
  end
end
