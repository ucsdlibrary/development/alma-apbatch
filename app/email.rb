require "date"
require "mail"
require_relative "../config/email"
require_relative "logger"

module App
  class Email
    ##
    # @param csv_attachment [String]
    # @param json_attachment [String]
    # @param xml_attachment [StringIO]
    # @param xml_location [StringIO]
    def self.send(csv_attachment:, json_attachment:, xml_location:, xml_attachment:)
      Logger.logger.info("Sending email for batch to #{ENV.fetch("EMAIL_TO")}")
      Mail.deliver do
        from ENV.fetch("EMAIL_FROM")
        to ENV.fetch("EMAIL_TO")
        subject "AP Batch Submission Success: #{Date.today}"
        body ["Please find the original Alma XML, the formatted Oracle JSON, and a CSV report attached.\n\n",
          "The file has been moved to: #{xml_location}"]
        add_file filename: "oracle-export.json", content: json_attachment
        add_file filename: "alma-export.xml", content: xml_attachment
        add_file filename: "csv-report.csv", content: csv_attachment
      end
      Logger.logger.info("Email delivered to #{ENV.fetch("EMAIL_TO")}")
    end

    ## Send a failure email to users
    # @param xml_location [String] Location of file that failed processing
    # @param errors [Array[RuleException]] List of errors that occurred during processing
    def self.send_error(xml_location:, errors:)
      Logger.logger.info("Sending error email for batch to #{ENV.fetch("EMAIL_TO")}")
      errors_ouput = errors.join("\n**********************\n")
      Mail.deliver do
        from ENV.fetch("EMAIL_FROM")
        to ENV.fetch("EMAIL_TO")
        subject "AP Batch Submission Failure: #{Date.today}"
        body ["Failed to process XML file: #{xml_location}\n\n",
          "There were #{errors.count} validation errors in the file\n\n",
          "Please see errors.txt attachment for more information"]
        add_file filename: "errors.txt", content: errors_ouput
      end
      Logger.logger.info("Failure Email delivered to #{ENV.fetch("EMAIL_TO")}")
    end
  end
end
