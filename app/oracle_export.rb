require "json"
##
# Responsible for generating Oracle JSON output to submit to the API
module App
  class OracleExport
    INVOICES_KEY = "ApInvoicesInterface".freeze
    INVOICE_LINES_KEY = "ApInvoiceLinesInterface".freeze

    attr_reader :invoices

    def initialize(invoices:)
      @invoices = invoices
    end

    #
    # Convert data to Oracle invoice JSON format
    def to_oracle_invoice
      build_data.to_json
    end

    # Build Hash in oracle invoice data structure
    def build_data
      data = []
      data << {}.tap { |pro| pro[INVOICES_KEY] = invoices.map { |inv| inv.to_h.compact } }
      data << {}.tap do |pro|
        inv_lines = invoices.map { |inv| inv.invoice_lines }
        pro[INVOICE_LINES_KEY] = inv_lines.flatten.sort_by! { |k| k.lineNumber.to_i }.map { |inv_line| inv_line.to_h.compact }
      end
    end
  end
end
