require "json"
require "net/http"
require "singleton"
require "uri"
require_relative "logger"

module App
  class Upload
    include Singleton
    def initialize
      @access_token ||= token
    end

    ##
    # Return the Upload class to use for `load` step in App:Transaction
    def self.for
      if ENV["NULL_UPLOAD"]
        NullUpload.new
      else
        instance
      end
    end

    ##
    # @param json [String]
    def send(json_attachment:)
      Logger.logger.info("Uploading Json Payload to OFC #{json_attachment}")
      uri = URI(ENV.fetch("OFC_API_URL"))
      req = Net::HTTP::Post.new(uri)
      req["Content-Type"] = "application/json"
      req["Authorization"] = "Bearer #{@access_token}"
      req.body = json_attachment

      begin
        response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == "https") { |http|
          http.request(req)
        }

        if response.is_a?(Net::HTTPSuccess)
          Logger.logger.info("Successful Posted Data #{response.body}") if response.code == "200"
        else
          Logger.logger.error("Failed to post Data. Response Code: #{response.code}")
        end
      end
    rescue => e
      Logger.logger.error("Failed to post Data. Response Code: #{response.code}")
      Logger.logger.error(e)
    end

    def token
      secret = ENV.fetch("OFC_CLIENT_SECRET")
      uri = URI(ENV.fetch("OFC_TOKEN_URL") + "?grant_type=client_credentials")
      req = Net::HTTP::Post.new(uri)
      req["Authorization"] = "Basic #{secret}"
      req["Content-Type"] = "application/x-www-form-urlencoded"
      req.set_form_data("grant_type" => "client_credentials")
      req.body = {}.to_json

      begin
        response = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == "https", verify_mode: OpenSSL::SSL::VERIFY_NONE) { |http|
          http.request(req)
        }

        if response.is_a?(Net::HTTPSuccess)
          Logger.logger.info("Successful requested access_token #{response.body}") if response.code == "200"
          JSON.parse(response.body)["access_token"]
        else
          Logger.logger.error("Failed to get access_token. Response Code: #{response.code}")
        end
      end
    rescue => e
      Logger.logger.error("Failed to get access_token. Response Code: #{response.code}")
      Logger.logger.error(e)
    end

    ## For use in local development to not send data to Oracle API
    # Targets `NULL_UPLOAD` environment variable
    class NullUpload
      def send(*args)
        Logger.logger.info("NullUpload: NOT uploading Json Payload to OFC")
      end
    end
  end
end
