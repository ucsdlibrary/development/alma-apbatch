require "dry-transaction"
require "opentelemetry/sdk"
require_relative "csv_report"
require_relative "email"
require_relative "extract"
require_relative "logger"
require_relative "oracle_export"
require_relative "s3_client"
require_relative "transform"
require_relative "upload"

module App
  class Transaction
    include Dry::Transaction

    def self.call(...)
      new.call(...)
    end

    step :extract # download from s3 bucket
    step :transform # transform into application data structure
    step :load # load into campus Oracle system
    step :rename # rename/move the s3 bucket file if everything has succeeded
    step :notify # email users with notification and attachments

    private

    def extract(input)
      span = OpenTelemetry::Trace.current_span
      span.add_event("Begin extract transaction step", attributes: {"filename" => input.key})
      @xml_data = S3Client.client.get_object(bucket: ENV.fetch("MINIO_BUCKET"), key: input.key).body.read
      Success(input)
    rescue Aws::S3::Errors::ServiceError => e
      msg = "Failed to extract #{input.key}"
      Logger.logger.error(msg)
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
      Failure(["#{msg}\n\n#{e}"])
    end

    def transform(input)
      span = OpenTelemetry::Trace.current_span
      span.add_event("Begin transform transaction step", attributes: {"filename" => input.key})
      @transformer = Transform.new(file: @xml_data)
      @transformer.extract_invoices
      if @transformer.invoices.empty?
        Logger.logger.error("#{input.key} has NO invoices inside it")
        Failure(["No invoices inside file #{input.key}"])
      elsif @transformer.flattened_errors.size > 0
        Logger.logger.error("#{input.key} has validation errors #{@transformer.errors.flatten}")
        Failure(@transformer.flattened_errors)
      else
        Success(input)
      end
    rescue => e
      msg = "Failed to transform #{input.key}"
      Logger.logger.error(msg)
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
      Failure(["#{msg}\n\n#{e}"])
    end

    def load(input)
      span = OpenTelemetry::Trace.current_span
      span.add_event("Begin load transaction step", attributes: {"filename" => input.key})
      @oracle_json = OracleExport.new(invoices: @transformer.invoices).to_oracle_invoice
      Upload.for.send(json_attachment: @oracle_json)
      Success(input)
    rescue => e
      msg = "Failed to load data into Oracle API for #{input.key}"
      Logger.logger.error(msg)
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
      Failure(["#{msg}\n\n#{e}"])
    end

    def rename(input)
      span = OpenTelemetry::Trace.current_span
      span.add_event("Begin rename transaction step", attributes: {"filename" => input.key})
      Extract.new.rename_processed_file(key: input.key,
        new_prefix: ENV.fetch("MINIO_PROCESSED_KEY_PREFIX"))
      Success(input)
    rescue => e
      msg = "Failed to rename processed file #{input.key}"
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
      Logger.logger.error(msg)
      Failure(["#{msg}\n\n#{e}"])
    end

    def notify(input)
      span = OpenTelemetry::Trace.current_span
      span.add_event("Begin notify transaction step", attributes: {"filename" => input.key})
      csv_report = CsvReport.create(invoices: @transformer.invoices)
      xml_location = "#{ENV.fetch("MINIO_PROCESSED_KEY_PREFIX")}/#{input.key}"
      Email.send(json_attachment: @oracle_json,
        csv_attachment: csv_report,
        xml_location: xml_location,
        xml_attachment: @xml_data)
      Success(input)
    rescue => e
      msg = "Failed to notify users for #{input.key}"
      Logger.logger.error(msg)
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
      Failure(["#{msg}\n\n#{e}"])
    end
  end
end
