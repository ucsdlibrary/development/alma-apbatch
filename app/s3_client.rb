require "aws-sdk-s3"

module App
  class S3Client
    def self.client
      @client ||= Aws::S3::Client.new(
        {
          endpoint: ENV.fetch("MINIO_ENDPOINT"),
          access_key_id: ENV.fetch("MINIO_ROOT_USER"),
          secret_access_key: ENV.fetch("MINIO_ROOT_PASSWORD"),
          region: "us-east-1", # just to satisfy the library, not used.
          force_path_style: true # needed for working with Minio buckets
        }
      )
    end
  end
end
