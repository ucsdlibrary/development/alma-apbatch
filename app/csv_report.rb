require "csv"

module App
  class CsvReport
    HEADERS = ["Invoice Number", "Invoice Total", "Line Number", "Line Amount", "Task", "Project", "COA"].freeze

    # Create the CSV report object for email submission
    # @param invoices [Array[Invoice]]
    # @return [String]
    def self.create(invoices:)
      CSV.generate(headers: true) do |csv|
        csv << HEADERS
        invoices.each do |invoice|
          invoice.invoice_lines.each do |invoice_line|
            csv << invoice_row(invoice: invoice, invoice_line: invoice_line)
          end
        end
      end
    end

    # Create a single invoice row for the CsvReport
    # @param invoice [Invoice]
    # @param invoice_line [InvoiceLine]
    # @return [Array]
    def self.invoice_row(invoice:, invoice_line:)
      [invoice.invoiceNum,
        invoice.invoiceAmount,
        invoice_line.lineNumber,
        invoice_line.amount,
        invoice_line.pjcTaskNumber,
        invoice_line.pjcProjectNumber,
        invoice_line.distCodeConcatenated]
    end
  end
end
