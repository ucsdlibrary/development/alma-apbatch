require "aws-sdk-s3"
require "opentelemetry/sdk"
require_relative "email"
require_relative "logger"
require_relative "transaction"
require_relative "s3_client"

##
# Connect to a Minio/S3 bucket which holds the Alma XML export files for AP Batch processing
module App
  class Extract
    def run
      span = OpenTelemetry::Trace.current_span
      Logger.logger.info("Initiating extraction of Alma XML files")
      S3Client.client.list_objects_v2(
        bucket: ENV.fetch("MINIO_BUCKET"),
        prefix: ENV.fetch("MINIO_KEY_PREFIX")
      ).contents.each do |xml_file|
        next if xml_file.key.match?(/#{ENV.fetch("KEEP_FILE")}/)
        Logger.logger.info("Begin extraction of XML file: #{xml_file.key}")
        span.add_event("Begin extraction of XML file", attributes: {"filename" => xml_file.key})

        Transaction.call(xml_file) do |result|
          result.success do
            Logger.logger.info("Successfully processed #{xml_file.key}")
            span.add_event("Successfully completed transaction process", attributes: {"filename" => xml_file.key})
          end
          result.failure do |failure|
            msg = "Failed to process #{xml_file.key}"
            Logger.logger.error(msg)
            Logger.logger.error(failure)
            span.status = OpenTelemetry::Trace::Status.error(msg + "\n\n" + failure.join)
            errors_prefix = ENV.fetch("MINIO_ERRORS_KEY_PREFIX")
            Logger.logger.info("Moving failed file #{xml_file.key} to #{errors_prefix}")
            rename_processed_file(key: xml_file.key,
              new_prefix: errors_prefix)
            Logger.logger.info("Sending email to users about failed file #{xml_file.key}")
            Email.send_error(xml_location: "#{errors_prefix}/#{xml_file.key}",
              errors: failure)
          end
        end
      end
    rescue Aws::S3::Errors::ServiceError => e
      msg = "Failed to list objects for bucket #{ENV.fetch("MINIO_BUCKET")}"
      Logger.logger.error(msg)
      Logger.logger.error(e)
      Logger.logger.error(e.backtrace)
      span.status = OpenTelemetry::Trace::Status.error(msg)
      span.record_exception(e)
    end

    # Rename an S3Object/file once it is processed
    # see: https://docs.aws.amazon.com/sdk-for-ruby/v3/api/Aws/S3/Object.html#move_to-instance_method
    def rename_processed_file(key:, new_prefix:)
      Logger.logger.info("Renaming processed file #{key}")
      # Construct an Aws::S3::Object which gives us the ability to #move_to (rename) the file
      aws_object_for_file = Aws::S3::Object.new(
        key: key,
        bucket_name: ENV.fetch("MINIO_BUCKET"),
        client: S3Client.client
      )
      aws_object_for_file.move_to(
        bucket: ENV.fetch("MINIO_BUCKET"),
        key: key.sub(
          ENV.fetch("MINIO_KEY_PREFIX"),
          new_prefix
        )
      )
    end
  end
end
