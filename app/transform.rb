require "nokogiri"
require "opentelemetry/sdk"
require_relative "../models/invoice"
require_relative "../models/invoice_line"
require_relative "../rules/rules"
require_relative "logger"

module App
  class Transform
    attr_reader :file, :errors
    ##
    # param file [StringIO]
    def initialize(file:)
      @file = file
      @errors = []
    end

    def flattened_errors
      errors.flatten
    end

    # Primary method to being Alma XML file extraction for mapping to Invoices
    def extract_invoices
      span = OpenTelemetry::Trace.current_span
      alma_doc = Nokogiri::XML(file)

      alma_doc.css("invoice").each do |i|
        invoice = Invoice.new
        errors << Rules.apply(type: InvoiceRule, record: invoice, xml_doc: i)
        invoice.invoice_lines = extract_invoice_lines(
          invoice: invoice,
          invoice_xml: i
        )

        invoices << invoice
      end
      msg = "Extracted #{invoices.count} invoices for batch with #{errors.size} errors"
      Logger.logger.info(msg)
      span.add_event(msg)
    end

    # Array that holds the set of Invoice records for this Alma XML file / batch
    def invoices
      @invoices ||= []
    end

    # Extract the invoice lines for a given invoice
    def extract_invoice_lines(invoice:, invoice_xml:)
      invoice_xml.css("invoice_line")
        .select { |line| is_valid_line_type?(line) }
        .map do |line|
        invoice_line = InvoiceLine.new
        apply_inherited_invoice_properties(
          invoice: invoice,
          invoice_line: invoice_line
        )
        errors << Rules.apply(type: InvoiceLineRule, record: invoice_line, xml_doc: line)
        errors << Rules.apply(type: InvoiceLineTaxRule, record: invoice_line, xml_doc: invoice_xml)
        invoice_line
      end
    end

    # Some InvoiceLine properties will be assigned directly from their
    # parent Invoice record
    def apply_inherited_invoice_properties(invoice:, invoice_line:)
      invoice_line.invoiceId = invoice.invoiceId
      invoice_line.pjcExpenditureItemDate = invoice.invoiceDate
    end

    private

    ##
    # Filter out invoice lines that we consider invalid
    # Invalid invoice lines have an line_type other than "REGULAR"
    # @param [Nokogiri::XML::Element] line
    def is_valid_line_type?(line)
      line.at_css("total_price").text != "0.0"
    end
  end
end
