# Alma APBatch

A new Alma version of the apbatch invoice payment/billing application

## Local Development

### Setup your .env file

You can start by copying the `.env.sample` file to `.env`

```sh
cp .env.sample .env
```

Then you can update the `.env` settings to your liking for testing. If the
`SMTP`-prefixed variables are NOT set, then the application will use the
`letter_opener` gem to persist the email to a local file and output the
location.

The SMTP account settings used for production are stored in Lastpass in an entry
called: `Alma apbatch email account`

Example:

```sh
$ bin/run
...
...
...
/dev/null file:////apbatch/tmp/letter_opener/1637262160_1674454_8196ac1/plain.html

$ cat tmp/letter_opener/1637262160_1674454_8196ac1/plain.html
```

### OpenTelemetry

The docker-compose development environment adds a [jaeger][jaeger] service which acts as a local [open telemetry][otel]
collector and UI.

It will be accessible at: `http://localhost:16686/` and can be used to check trace, span, event, and attribute
information locally.

#### Production

Production observability data is published to [Honeycomb][honeycomb]


### Using Minio with docker-compose
It may sometimes be useful to test out the entire transaction process for the Ap
Batch application.

In this scenario, there is a provided `docker-compose.yaml` file that can be
used. There are a few steps involved here:

```sh
docker-compose build # or bin/build
docker-compose up
```

Then you can proceed to test out and run the entire ETL stack (optionally
submitting to the Oracle QA API as well).



### Build Docker image

To build an image for the project, you can use the helper `bin/build` script as
follow:

```sh
bin/build
```

### Run Docker image

To run a built image for the project, you can use the helper `bin/run` script as
follow:

```sh
bin/run
```

### Ran A Shell In The Docker image

To run a local terminal shell for the built image, you can use the helper `bin/shell` script as
follow:

```sh
bin/shell
```

### Lint code In The Docker image

To run the standard linter for the built image, you can use the helper `bin/lint` script as
follow:

```sh
bin/lint
```

### Test code In The Docker image

To run rspec tests for the built image, you can use the helper `bin/test` script as
follow:

```sh
bin/test
```

### Run a one-off Job from the CronJob

To run manually a job based on the cronJob to make sure things work as expected from review/staging environment, you can push the "Run" button in CI Jobs list or create kubectl command.

You can use Gitlab CI as follow:

```
Select Build -> Jobs 
click the "Run" button for the job  #somenumber: alma-apbatch:staging:one-time-job
```

You can also create a one-time-job as follow:

```
$ kubectl create job --namespace=alma-apbatch-prod --from="cronjob/alma-apbatch-cron" "alma-apbatch-otj"
```

To view the logs from a CronJob that runs, find the Job that was created from the CronJob and view the logs as follow:

```
$ kubectl --namespace=alma-apbatch-prod logs job/alma-apbatch-cron-<<job-id>>
Example: kubectl --namespace alma-apbatch-prod logs job/alma-apbatch-cron-27527550
```

[honeycomb]:https://ui.honeycomb.io/surfliner/environments
