# Data structure representing a single Invoice record extracted from an Alma XML export
Invoice = Struct.new(
  :invoiceId, :businessUnit, :source, :invoiceNum, :invoiceAmount,
  :invoiceDate, :vendorNum, :vendorSiteCode,
  :termsName, :calcTaxDuringImportFlag, :addTaxToInvAmtFlag,
  :invoiceCurrencyCode, :paymentCurrencyCode, :invoiceType,
  :vendorName
) do
  # TODO: test association of Invoice data structure with hasMany InvoiceLines
  attr_reader :invoice_lines
  def invoice_lines=(invoice_lines)
    @invoice_lines ||= invoice_lines
  end
end
