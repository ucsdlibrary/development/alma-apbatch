# Data structure representing a single Invoice Line record extracted from an Alma XML export
InvoiceLine = Struct.new(
  :invoiceId, :amount, :lineNumber, :pjcExpenditureItemDate,
  :distCodeConcatenated, :pjcProjectNumber, :pjcTaskNumber, :pjcExpenditureTypeName,
  :lineTypeLookupCode, :invoiceCurrencyCode, :paymentCurrencyCode, :pjcOrganizationName,
  :shipToLocationCode, :attribute12
)
