Dir.glob(File.join(File.dirname(__FILE__), "{invoice,invoice_line,.}", "**", "*.rb"), &method(:require))

class Rules
  ##
  # Rules registry (of sorts) which provides a single entry point into applying rules of a given `type`
  # @param type [Rule] Any class which implemented the Rule interface. Examples: InvoiceRule and InvoiceLineRule
  # @param record [Struct] Examples: Invoice and InvoiceLine
  # @param xml_doc [Nokogiri::XML::Element] An XML element from which to select, query, etc.
  def self.apply(type:, record:, xml_doc:)
    errors = []
    type.descendants.each do |r|
      r.call(record: record, xml_doc: xml_doc)
    rescue RuleException => e
      errors.push(e.message)
    end
    # we need to explicitly return the errors, rubocop/standard is wrong here..
    # rubocop:disable Style/RedundantReturn
    return errors.flatten
    # rubocop:enable Style/RedundantReturn
  end
end
