require_relative "../rule"

class ChargeOfAccount < InvoiceLineRule
  def self.call(record:, xml_doc:)
    raise RuleException.new("external_id is not present in invoice line\n\n#{xml_doc}") unless xml_doc.at_css("fund_info_list fund_info external_id")

    external_id = xml_doc.at_css("fund_info_list fund_info external_id").text.split("|")
    record.distCodeConcatenated = external_id[0]
    record.pjcProjectNumber = external_id[1]
    record.pjcTaskNumber = external_id[2]
    record.pjcExpenditureTypeName = external_id[4]
  end
end
