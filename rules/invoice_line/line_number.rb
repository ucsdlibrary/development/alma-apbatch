require_relative "../rule"

class LineNumber < InvoiceLineRule
  def self.call(record:, xml_doc:)
    raise RuleException.new("line_number is not present in invoice line\n\n#{xml_doc}") unless xml_doc.at_css("line_number")

    record.lineNumber = xml_doc.at_css("line_number").text
  end
end
