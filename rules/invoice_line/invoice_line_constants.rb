require_relative "../rule"

# Holds a set of hard coded InvoiceRule properties
class InvoiceLineConstants < InvoiceLineRule
  def self.call(record:, xml_doc:)
    record.lineTypeLookupCode = "ITEM"
    record.invoiceCurrencyCode = "USD"
    record.paymentCurrencyCode = "USD"
    record.pjcOrganizationName = "Library Collections"
  end
end
