require_relative "../rule"

# Holds a set of hard coded InvoiceRule properties
class InvoiceLineTaxConstants < InvoiceLineTaxRule
  def self.call(record:, xml_doc:)
    taxable = xml_doc.css("reporting_code").to_s.include?(">taxable<")
    if taxable
      record.shipToLocationCode = "UCSD"
      record.attribute12 = "No"
    end
  end
end
