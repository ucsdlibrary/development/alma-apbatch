require_relative "../rule"

class Amount < InvoiceLineRule
  def self.call(record:, xml_doc:)
    raise RuleException.new("total_price is not present in invoice line\n\n#{xml_doc}") unless xml_doc.at_css("total_price")

    record.amount = xml_doc.at_css("total_price").text
  end
end
