# Base Rule interface for Rules to adopt
# A simple class .call method which makes an invoice assignment/mapping given the Nokogiri xml document
class Rule
  ##
  # @param record [Struct]
  # @param xml_doc [Nokogiri::XML::Element]
  def self.call(record:, xml_doc:)
    puts "interface method. must be implemented.."
  end

  # Lookup the classes which are children of this class.
  # This allows us to dynamically call `.apply` on `InvoiceRules` and `InvoiceLineRules`
  # This means registering a new Rule for the application just means extending on of the subclasses below, and
  # implementing the `.call` method
  def self.descendants
    ObjectSpace.each_object(Class).select { |klass| klass < self }
  end
end

# Set of Rule's that should be applied to an Invoice record
class InvoiceRule < Rule; end

# Set of Rule's that should be applied to an InvoiceLine record
class InvoiceLineRule < Rule; end

# Set of Rule's that should be applied to an InvoiceLine taxable record
class InvoiceLineTaxRule < Rule; end

# Named rule exceptions for handling
class RuleException < StandardError
end
