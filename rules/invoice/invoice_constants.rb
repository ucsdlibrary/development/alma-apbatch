require_relative "../rule"

# Holds a set of hard coded InvoiceRule properties
class InvoiceConstants < InvoiceRule
  def self.call(record:, xml_doc:)
    record.invoiceCurrencyCode = "USD"
    record.paymentCurrencyCode = "USD"
    record.invoiceType = "STANDARD"
    record.vendorName = ""
    record.source = "UCSD Int Library Sys"
    record.businessUnit = "UCSD CAMPUS"
    taxable = xml_doc.css("reporting_code").to_s.include?(">taxable<")
    if taxable
      record.termsName = "Immediate"
      record.calcTaxDuringImportFlag = "N"
      record.addTaxToInvAmtFlag = "N"
    end
  end
end
