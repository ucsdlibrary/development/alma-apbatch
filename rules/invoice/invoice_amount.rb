require_relative "../rule"

class InvoiceAmount < InvoiceRule
  def self.call(record:, xml_doc:)
    raise RuleException.new("invoice_amount sum is not present in invoice\n\n#{xml_doc}") unless xml_doc.at_css("invoice_amount sum")
    record.invoiceAmount = xml_doc.at_css("invoice_amount sum").text
  end
end
