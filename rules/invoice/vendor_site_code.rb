require_relative "../rule"

class VendorSiteCode < InvoiceRule
  def self.call(record:, xml_doc:)
    raise RuleException.new("vendor_additional_code is not present in invoice\n\n#{xml_doc}") unless xml_doc.at_css("vendor_additional_code")

    record.vendorSiteCode = xml_doc.at_css("vendor_additional_code").text
  end
end
