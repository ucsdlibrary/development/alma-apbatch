require "date"
require_relative "../rule"

##
# Transform a date of the form: 05/31/2021 into an iso8601 date
class InvoiceDate < InvoiceRule
  def self.call(record:, xml_doc:)
    raise RuleException.new("invoice_date is not present in invoice\n\n#{xml_doc}") unless xml_doc.at_css("invoice_date")

    invoice_date_xml = xml_doc.at_css("invoice_date").text
    record.invoiceDate = Date.strptime(invoice_date_xml, "%m/%d/%Y").iso8601
  end
end
