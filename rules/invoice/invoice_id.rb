require_relative "../rule"

class InvoiceId < InvoiceRule
  def self.call(record:, xml_doc:)
    raise RuleException.new("unique_identifier is not present in invoice\n\n#{xml_doc}") unless xml_doc.at_css("unique_identifier")

    invoice_id = xml_doc.at_css("unique_identifier").text
    max_length = (invoice_id.length > 15) ? 15 : invoice_id.length
    record.invoiceId = invoice_id[1, max_length]
  end
end
