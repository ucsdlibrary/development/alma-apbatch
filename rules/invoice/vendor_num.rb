require_relative "../rule"

class VendorNum < InvoiceRule
  def self.call(record:, xml_doc:)
    raise RuleException.new("vendor_FinancialSys_Code is not present in invoice\n\n#{xml_doc}") unless xml_doc.at_css("vendor_FinancialSys_Code")

    record.vendorNum = xml_doc.at_css("vendor_FinancialSys_Code").text
  end
end
