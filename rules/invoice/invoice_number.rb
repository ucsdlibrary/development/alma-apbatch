require_relative "../rule"

##
# This rule maps the value of `invoice_number` which is unique to the Library identifier pool, and adds an extension to
# ensure it is unique across all invoice identifiers for the campus system
# For our purposes, both the invoiceId AND the invoiceNum share this same value
class InvoiceNumber < InvoiceRule
  def self.call(record:, xml_doc:)
    raise RuleException.new("invoice_number is not present in invoice\n\n#{xml_doc}") unless xml_doc.at_css("invoice_number")
    invoice_number = xml_doc.at_css("invoice_number").text + "-LIB"
    record.invoiceNum = invoice_number
  end
end
